<?php

/**
 * @Author: TORTIK
 */

namespace TORTIK\CancelOrder\Controller\Api;

use Exception;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use TORTIK\CancelOrder\Api\CancelOrderInterface;

class CancelOrder extends \Magento\Framework\App\Action\Action
{
    protected $orderCancel;
    protected $jsonFactory;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        CancelOrderInterface $orderCancel
    ) {
        $this->jsonFactory = $jsonFactory;
        $this->orderCancel = $orderCancel;
        parent::__construct($context);
    }

    public function execute()
    {
        try{
        $orderId = $this->getRequest()->getParam('order_id');
        $resultJson = $this->jsonFactory->create();

        $response = $this->orderCancel->cancelOrder($orderId);
        
        return $resultJson->setData($response);
        //$resultJson->setData($response)
        } catch (Exception $e){
            return $resultJson->setData($response);
        }
    }
}
