<?php

/**
 * @Author: TORTIK
 */

namespace TORTIK\CancelOrder\Model\Api;

use Psr\Log\LoggerInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use TORTIK\CancelOrder\Api\CancelOrderInterface;
use Magento\Sales\Model\Order;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\Pricing\Helper\Data;
use Magento\Framework\Mail\Template\TransportBuilder;

class OrderCancel implements CancelOrderInterface
{
    protected $orderModel;
    protected $jsonFactory;
    protected $logger;
    private   $transportBuilder;
    protected $priceHelper;
    protected $customer;
    protected $helper;
    protected $collectionFactory;


    public function __construct(
        Order $orderModel,
        LoggerInterface $logger,
        JsonFactory $jsonFactory,
        CollectionFactory $collectionFactory,
        Data $priceHelper,
        TransportBuilder $transportBuilder,
        \TORTIK\CancelOrder\Helper\Api\Data $helper
    ) {
        $this->priceHelper       = $priceHelper;
        $this->transportBuilder  = $transportBuilder;
        $this->helper            = $helper;
        $this->collectionFactory = $collectionFactory;
        $this->jsonFactory       = $jsonFactory;
        $this->orderModel        = $orderModel;
        $this->logger            = $logger;
    }

    /**
     * {@inheritdoc}
     */

    public function cancelOrder($orderId)
    {
        try {
            $order = $this->orderModel->load($orderId);
            $resultJson = $this->jsonFactory->create();
            $productId = [];
            foreach ($order->getAllItems() as $item) {
                $productId[] = $item->getProductId();
            }
            $productCollection = $this->collectionFactory->create();
            $productCollection->addAttributeToSelect('*')->addFieldToFilter('entity_id', array('in' => $productId));
                $products = [];
                foreach ($productCollection as $product) {
                    $products[] = $product;              
    
                }    
            $post['collectionProduct'] = $products;
            if ($order->canCancel()) {
                $order->cancel();
                $order->save();
                $post['store_id'] = $order->getStore()->getStoreId();
                $post['store_name'] = $order->getStore()->getName();
                $post['site_name'] = $order->getStore()->getWebsite()->getName();
                $post['entity_id'] = $order->getEntity_id();
                $post['base_grand_total'] = $this->priceHelper->currency($order->getBase_grand_total(), true, false);
                $post['created_at'] = $order->getCreated_at();
                $post['customer_name'] = $order->getCustomerName();
                $post['orderid'] = $order->getIncrement_id();
                $senderName = $order->getCustomerName();
                $senderEmail = $order->getCustomerEmail();
                $sender = [
                    'name' => $senderName,
                    'email' => $this->helper->getEmailSender(),
                    ];
                if($this->helper->getEmailSender()){
                    if($this->helper->getEmailSeller()){
                        $transport = $this->transportBuilder->setTemplateIdentifier('cancel_order_email_template')
                        ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $post['store_id']])
                        ->setTemplateVars($post)
                        ->setFrom($sender)
                        ->addTo($senderEmail)
                        ->addCc($this->helper->getEmailSeller())           
                        ->getTransport();               
                    }else {
                        $transport = $this->transportBuilder->setTemplateIdentifier('cancel_order_email_template')
                        ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $post['store_id']])
                        ->setTemplateVars($post)
                        ->setFrom($sender)
                        ->addTo($senderEmail)       
                        ->getTransport();                              
                    }
                    try {
                        $transport->sendMessage();
                    } catch (\Exception $e) {
                        $this->logger->critical($e->getMessage());
                    }    
                }    
                return ['success' => true, 'refresh_page' => true];
            } else {
              return ['success' => false, 'refresh_page' => false];
            }
        } catch (\Exception $e) {
          return ['success' => false, 'refresh_page' => false];
        }
    }

}
