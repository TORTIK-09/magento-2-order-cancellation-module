<?php

  /**
  * @Author: TORTIK
  */

namespace TORTIK\CancelOrder\Api;
 
interface CancelOrderInterface
{
    /**
     * Принимаем идентификатор заказа и возвращаем массив строк
     *
     * @param int $orderId
     * @return string[]
     */
    public function cancelOrder($orderId);
}