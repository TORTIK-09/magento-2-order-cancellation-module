import axios from 'axios';

const apiUrl = process.env.VUE_RESTAPI_URL + 'V1/order/cancel/';

export const cancelOrder = async (orderId) => {
  const url = `${apiUrl}${orderId}`;
  const response = await axios.post(url, { orderid: orderId }, {
    headers: {
      'Content-Type': 'application/json'
    }
  });
  return response.data;
};
